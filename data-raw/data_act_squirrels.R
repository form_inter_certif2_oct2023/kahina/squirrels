# 1ere etape : la lecture des donnees
nyc_squirrels_act_sample <- readr::read_csv(
  file = "data-raw/nyc_squirrels_act_sample.csv"
)

data_act_squirrels <- nyc_squirrels_act_sample |>
  dplyr::slice_sample(n = 15)

# 2 eme etape : Ajouter les donnees dans /data
usethis::use_data(data_act_squirrels, overwrite = TRUE)

# 3 eme etape : La creation de ma documentation
checkhelper::use_data_doc("data_act_squirrels")
attachment::att_amend_desc()
